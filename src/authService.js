// error code 1 - no account
// error code 2 - wrong password
// error code 3 - no response
// error code 4 - internal server error

export const signIn = (email, password) =>
    new Promise((resolve, reject) => {
        setTimeout(() => {
            if (email !== "admin@admin.com") {
                reject({ code: 1 });
            }

            if (password !== "admin") {
                reject({ code: 2 });
            }

            resolve();
        }, 1500);
    });