# XState Sign-In Form
Example sign-in form built with XState, React & Atlaskit.

## Getting started
```bash
# install dependencies
yarn

# start app local server
yarn start
```

## Licensing
[MIT](https://choosealicense.com/licenses/mit/)